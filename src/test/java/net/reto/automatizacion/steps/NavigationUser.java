package net.reto.automatizacion.steps;

import static org.assertj.core.api.Assertions.assertThat;

import net.reto.automatizacion.ui.CurrentPage;
import net.reto.automatizacion.ui.PlazaVeaPage;
import net.thucydides.core.annotations.Step;

public class NavigationUser {

	PlazaVeaPage plazaVeaPage;
	CurrentPage  currentPage ;
	
	@Step
	public void openStartPage() {
		plazaVeaPage.open();		
		
	}

	public void isHomePage(String title) {		
		assertThat(currentPage.getTitle()).containsIgnoringCase(title);
	}

}
