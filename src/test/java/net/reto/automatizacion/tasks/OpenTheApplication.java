package net.reto.automatizacion.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import net.reto.automatizacion.ui.PlazaVeaPage;

public class OpenTheApplication implements Task {

	PlazaVeaPage plazaVeaPage;

    @Step("Open the application")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(plazaVeaPage)
        );
    }
}
