package net.reto.automatizacion.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static org.openqa.selenium.Keys.ENTER;

import net.reto.automatizacion.ui.AddCart;
import net.reto.automatizacion.ui.Product;
import net.reto.automatizacion.ui.SearchBox;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class AddToCart implements Task {

	
	
	@Step("Add Product to Cart")
    public static AddToCart productSetupAdd( ) {
        return instrumented(AddToCart.class);
    }

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
				Click.on(Product.PRODUCT_ADD));
		
	}
}
