package net.reto.automatizacion.tasks;


import static org.assertj.core.api.Assertions.assertThat;

import org.openqa.selenium.By;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;


public class CartProductResult implements Question<String>{
	

	@Override
	public String answeredBy(Actor actor) {
		// TODO Auto-generated method stub
		String valueComponent = BrowseTheWeb.as(actor).find(By.className("cart__number")).getText();
		System.out.println("The valuie es = " + valueComponent);
		assertThat(valueComponent).contains("1");
		return valueComponent;
	}

	
    public static Question<String> resultCart() {
        return new CartProductResult();
    }


}
