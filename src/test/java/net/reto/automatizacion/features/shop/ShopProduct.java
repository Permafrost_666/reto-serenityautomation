package net.reto.automatizacion.features.shop;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.reto.automatizacion.steps.NavigationUser;
import net.reto.automatizacion.tasks.AddToCart;
import net.reto.automatizacion.tasks.CartProductResult;
import net.reto.automatizacion.tasks.OpenTheApplication;
import net.reto.automatizacion.tasks.Search;
import net.reto.automatizacion.tasks.SelectProduct;
import net.reto.automatizacion.ui.Cart;
import net.reto.automatizacion.tasks.AddToCart;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.EventualConsequence.eventually;

@RunWith(SerenityRunner.class)
public class ShopProduct {
	
	Actor sandra = Actor.named("Sandra");
	
	@Steps
	OpenTheApplication openTheApplication; 
	
	
	@Managed
	WebDriver herBrowser;	
	
    @Before
    public void sandraCanBrowseTheWeb() {
        sandra.can(BrowseTheWeb.with(herBrowser));
    }
	
		
    @Test
    public void addProductToCart() {
    	//Given 
    	givenThat(sandra).wasAbleTo(openTheApplication);

    	//When    	
    	 when(sandra).attemptsTo(Search.forTheTerm("Televisores"));
    	 and(sandra).attemptsTo(SelectProduct.goProduct());
    	 and(sandra).attemptsTo(AddToCart.productSetupAdd());
    	 
    	//Then
         then(sandra).asksFor(CartProductResult.resultCart());
    }
    
    @After
    public void closeTest() {
    	herBrowser.close();
    }

}
